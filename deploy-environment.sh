#!/bin/bash

environmentName="integration-${1:-1}"
rbdImage="${2:-$environmentName}"
goldenImage="$(kubectl get pvc -l app=mysql-breeder -o jsonpath='{.items[*].spec.volumeName}')"

# if the image was not allready cloned, clone it
if ! kubectl -n rook exec -it rook-tools -- rbd info replicapool/${rbdImage} >/dev/null; then
  echo "clone golden Image to $rbdImage"
  kubectl -n rook exec -it rook-tools -- rbd clone replicapool/${goldenImage}@goldenimage replicapool/${rbdImage}
fi

# create database
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: PersistentVolume
metadata:
  annotations:
    pv.kubernetes.io/provisioned-by: rook.io/block
  name: pvc-$environmentName-mysql
  labels:
    environment: $environmentName
    app: mysql
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 1Gi
  flexVolume:
    driver: rook.io/rook
    options:
      image: $rbdImage
      pool: replicapool
      storageClass: rook-block
  persistentVolumeReclaimPolicy: Delete
  storageClassName: rook-block

---

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: $environmentName-mysql
  labels:
    app: $environmentName-mysql
    type: mysql
    environment: $environmentName
    tier: database
spec:
  replicas: 1
  serviceName: mysql-breeder
  selector:
    matchLabels:
      tier: database
      app: $environmentName-mysql
      environment: $environmentName
  template:
    metadata:
      labels:
        app: $environmentName-mysql
        environment: $environmentName
        tier: database
    spec:
      containers:
      - name: mysql
        image: mysql:5.7
        args: ["--ignore-db-dir=lost+found"]
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: innomeet
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-data
          mountPath: /var/lib/mysql
  volumeClaimTemplates:
  - metadata:
      name: mysql-data
    spec:
      accessModes: ["ReadWriteOnce"]
      storageClassName: rook-block
      volumeName: pvc-$environmentName-mysql
      resources:
        requests:
          storage: 1Gi

---

apiVersion: v1
kind: Service
metadata:
  name: $environmentName-mysql
  labels:
    environment: $environmentName
    app: $environmentName-mysql
spec:
  ports:
  - name: http
    targetPort: mysql
    port: 3307
  selector:
    app: $environmentName-mysql
  type: NodePort
EOF
