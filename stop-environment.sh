#!/bin/sh

environmentName="integration-${1:-1}"

kubectl delete all -l environment=${environmentName}
kubectl delete pvc -l environment=${environmentName}
